## activate virtualenv

```bash
..\..\venv\Scripts\activate
```

## Get-Started

### create new django project

```bash
django-admin startproject <project-name>
cd <project-name>
```

### start django project

```bash
python manage.py runserver [ip:port]
```

### localize django project

edit the bottom of `<project-name>/settings.py`

```python
LANGUAGE_CODE = 'zh-Hans'
TIME_ZONE = 'Asia/Shanghai'
```

## Manage

### create a new module

```bash
python manage.py startapp <module-name>
```

You also need to include `<module-name>` in `<project-name>/settings.py` `INSTALLED_APPS`!

### create a data-table

1. add a new class/model in `<module-name>/models.py`
2. `python manage.py makemigrations mainsite`
3. `python manage.py migrate`

### create superuser and manage table

```bash
python manage.py createsuperuser
```

add follow text in `<module-name>/admin.py`

```python
from django.contrib import admin
from .models import <table-name>
admin.site.register(<table-name>)
```

Now, you can see admin-site in http://ip:port/admin and manage the table you have added.