from django.db import models
from django.utils import timezone

# Create your models here.

# Post：文章
# title:标题；slug：网址；body：内容；pub_date：发表时间
# __str__：便于在命令行测试代码时了解相关模型对象的细节
class Post(models.Model):
    title = models.CharField(max_length=200)
    slug = models.CharField(max_length=200)
    body = models.TextField()
    pub_date = models.DateTimeField(default=timezone.now)

    class Meta:
        ordering = ('-pub_date',)
    
    def __str__(self):
        return self.title
