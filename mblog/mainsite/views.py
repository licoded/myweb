from django.shortcuts import render
from django.http import HttpResponse
from .models import Post

# Create your views here.
def homepage(request):
    posts = Post.objects.all()
    posts_list = list()
    for count, post in enumerate(posts):
        posts_list.append(f'No.{count}: {post}<hr>')
        posts_list.append(f'<small>{post.body}</small><br><br>')
    return HttpResponse(posts_list)